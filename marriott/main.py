import json
import requests
import pandas as pd

if __name__ == '__main__':

	BASE_URL = "https://gatewaydsapprd.marriott.com"
	OFFSET = 0
	LIMIT = 10
	codes = pd.DataFrame({'id': [], 'href': [], 'matchesSavedProperties': [], 'links': []})
	df = pd.DataFrame()

	headers = {
		"Authorization": "Basic YnJhbmQtc2l0ZXM6UHM5MXN5VnZwSE5HWndrbEM3eEhwempRM3lqU2draktxaDQ3Uk4yOFZJZGVKbE5mN0ZqQ1ZycUh3bldVZmt4Zw==",
		"Accept-Encoding": "identity",
		"Accept-Language": "en-US",
		"Content-Type": "application/json",
		"Cache-Control": "no-cache"
	}

	while True:
		paths = [
			"/v2/queries/properties?offset=" + str(OFFSET) + "&AKAQA=qa13&limit=" + str(LIMIT) + "&include=properties.basic-information,properties.photos,properties.contact-information,properties.children-activities,properties.spa",
			"/MarriottService/v1/geographic-regions",
			"/v2/brands/WI/photos"
		]

		body = [
			{
				"queries": [
					{
						"id": "include-unavailable-properties",
						"values": [
							"true"
						]
					},
					{
						"id": "number-in-party",
						"values": [
							"1"
						]
					},
					{
						"id": "quantity",
						"values": [
							"1"
						]
					}
				],
				"facets": {
					"terms": [
						{
							"type": "amenities",
							"sort": "+value"
						},
						{
							"type": "meetings-and-events",
							"sort": "+value"
						},
						{
							"type": "transportation-types",
							"sort": "+value"
						},
						{
							"type": "property-types",
							"sort": "+value"
						},
						{
							"type": "brands",
							"dimensions": [
								"WI"
							]
						}
					]
				},
				"filters": [
					{
						"id": "portfolio",
						"values": [
							"SW",
							"MR"
						]
					}
				]
			}
		]

		response = requests.post(url=BASE_URL + paths[0], data=json.dumps(body[0]), headers=headers)

		if response.status_code == 200:
			OFFSET = OFFSET + LIMIT
			#print (str(OFFSET) + " OFFSET:" )
			#print(len(response.json()['properties']))
			for property in response.json()['properties']:
				#print(property.keys())
				df = pd.DataFrame.from_dict(property)
				print(df.head(100))
				#codes.update(test)
				#print(property['id'])
			#print(response.json()['properties'][0]['id'])

			#print(json.dumps(response.json(), indent=2))
			#df = pd.DataFrame.from_dict(j)
			#print(response.status_code)
			#print(response.content)
			#data2 = json.loads(response.content.decode("utf-8"))
			#df = pd.json_normalize(data2, "properties")
			#df.head(10)
			if len(response.json()['properties']) < LIMIT:
				break;
		else:
			print("Request failed")
			break;



