<?php
ini_set('memory_limit', '1028M');

$data = array();
//values and JSON values
$data['FirstName'] = 0; // 'First Name'
$data['FirstNameNull'] = 0; // 'First Name'
$data['LastName'] = 0; // 'Last Name'
$data['LastNameNull'] = 0; // 'Last Name'
$data['MailingState'] = 0; // 'Mailing State'
$data['MailingStateNull'] = 0; // 'Mailing State'
$data['MailingZip'] = 0; // 'Mailing Zip'
$data['MailingZipNull'] = 0; // 'Mailing Zip'
$data['MailingCounty'] = 0; // 'Mailing County'
$data['MailingCountyNull'] = 0; // 'Mailing County'
$data['ShippingState'] = 0; // 'Shipping State'
$data['ShippingStateNull'] = 0; // 'Shipping State'
$data['ShippingZip'] = 0; // 'Shipping Zip'
$data['ShippingZipNull'] = 0; // 'Shipping Zip'
$data['CornTrue'] = 0; // Corn
$data['CornFalse'] = 0;
$data['CornNull'] = 0;
$data['SoybeanTrue'] = 0; // Soybean
$data['SoybeanFalse'] = 0;
$data['SoybeanNull'] = 0;
$data['CottonTrue'] = 0; // Cotton
$data['CottonFalse'] = 0;
$data['CottonNull'] = 0;
$data['SpecialtyTrue'] = 0; // Specialty
$data['SpecialtyFalse'] = 0;
$data['SpecialtyNull'] = 0;
$data['MobileTrue'] = 0;
$data['MobileFalse'] = 0;
$data['MobileNull'] = 0;
$data['InsectTrue'] = 0; // Insect Forecast
$data['InsectFalse'] = 0;
$data['InsectNull'] = 0;
$data['MobilePhone'] = 0; // Mobile Phone
$data['MobilePhoneNull'] = 0; // Mobile Phone

$file = fopen("subscribers.json", "r");
while(! feof($file)) {
  $json = json_decode(fgets($file), true);
  foreach ($json['Attributes'] as $row) {
    if ($row['Name'] == 'First Name') {
      if ($row['Value'] != '') {
        $data['FirstName']++;
      } else {
        $data['FirstNameNull']++;
      }
    }

    if ($row['Name'] == 'Last Name') {
      if ($row['Value'] != '') {
        $data['LastName']++;
      } else {
        $data['LastNameNull']++;
      }
    }

    if ($row['Name'] == 'Mailing State') {
      if ($row['Value'] != '') {
        $data['MailingState']++;
      } else {
        $data['MailingStateNull']++;
      }
    }

    if ($row['Name'] == 'Mailing Zip') {
      if ($row['Value'] != '') {
        $data['MailingZip']++;
      } else {
        $data['MailingZipNull']++;
      }
    }

    if ($row['Name'] == 'Mailing County') {
      if ($row['Value'] != '') {
        $data['MailingCounty']++;
      } else {
        $data['MailingCountyNull']++;
      }
    }

    if ($row['Name'] == 'Shipping State') {
      if ($row['Value'] != '') {
        $data['ShippingState']++;
      } else {
        $data['ShippingStateNull']++;
      }
    }

    if ($row['Name'] == 'Shipping Zip') {
      if ($row['Value'] != '') {
        $data['ShippingZip']++;
      } else {
        $data['ShippingZipNull']++;
      }
    }

    if ($row['Name'] == 'Corn') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['CornNull']++;
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['CornFalse']++;
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['CornTrue']++;
      }
    }

    if ($row['Name'] == 'Soybean') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['SoybeanNull']++;
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['SoybeanFalse']++;
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['SoybeanTrue']++;
      }
    }

    if ($row['Name'] == 'Cotton') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['CottonNull']++;
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['CottonFalse']++;
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['CottonTrue']++;
      }
    }

    if ($row['Name'] == 'Specialty') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['SpecialtyNull']++;
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['SpecialtyFalse']++;
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['SpecialtyTrue']++;
      }
    }

    if ($row['Name'] == 'MobileCommunications') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['MobileNull']++;
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['MobileFalse']++;
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['MobileTrue']++;
      } elseif (preg_match('/would like/', $row['Value'])) {
        $data['MobileTrue']++;
      }
    }

    if ($row['Name'] == 'InsectForecast') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['InsectNull']++;
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['InsectFalse']++;
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['InsectTrue']++;
      } elseif (preg_match('/consent to/', $row['Value'])) {
        $data['InsectTrue']++;
      }
    }

    if ($row['Name'] == 'Mobile Phone') {
      if ($row['Value'] != '') {
        $data['MobilePhone']++;
      } else {
        $data['MobilePhoneNull']++;
      }
    }
  }
}
$csv = "First Name," . $data['FirstName'] . ',,' . $data['FirstNameNull'] . "\n";
$csv .= "Last Name," . $data['LastName'] . ',,' . $data['LastNameNull'] . "\n";
$csv .= "Mailing State," . $data['MailingState'] . ',,' . $data['MailingStateNull'] . "\n";
$csv .= "Mailing Zip," . $data['MailingZip'] . ',,' . $data['MailingZipNull'] . "\n";
$csv .= "Mailing County," . $data['MailingCounty'] . ',,' . $data['MailingCountyNull'] . "\n";
$csv .= "Shipping State," . $data['ShippingState'] . ',,' . $data['ShippingStateNull'] . "\n";
$csv .= "Shipping Zip," . $data['ShippingZip'] . ',,' . $data['ShippingZipNull'] . "\n";
$csv .= "Corn," . $data['CornTrue'] . ',' . $data['CornFalse'] . ',' . $data['CornNull'] . "\n";
$csv .= "Soybean," . $data['SoybeanTrue'] . ',' . $data['SoybeanFalse'] . ',' . $data['SoybeanNull'] . "\n";
$csv .= "Cotton," . $data['CottonTrue'] . ',' . $data['CottonFalse'] . ',' . $data['CottonNull'] . "\n";
$csv .= "Specialty," . $data['SpecialtyTrue'] . ',' . $data['SpecialtyFalse'] . ',' . $data['SpecialtyNull'] . "\n";
$csv .= "Mobile Comms," . $data['MobileTrue'] . ',' . $data['MobileFalse'] . ',' . $data['MobileNull'] . "\n";
$csv .= "Insect Forecast," . $data['InsectTrue'] . ',' . $data['InsectFalse'] . ',' . $data['InsectNull'] . "\n";
$csv .= "Mobile Phone," . $data['MobilePhone'] . ',,' . $data['MobilePhoneNull'] . "\n";
echo $csv;
fclose($file);

?>
