<?php
ini_set('memory_limit', '256M');

$fullList = array();

$file = fopen("traits.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $full = explode(',', $data);
  $fullList[trim($full[4])] = $data;
}
fclose($file);

$count = 1;
unlink('deduped-list-2021-12-03.csv');
foreach ($fullList as $line) {
  //echo $count . ": " . $line . "\n";
  file_put_contents('deduped-list-2021-12-03.csv', $line, FILE_APPEND);
  echo $line;
  $count++;
}
