<?php
ini_set('auto_detect_line_endings',TRUE);
$handle = fopen('full.csv','r');
$full_list = array();
while ( ($data = fgetcsv($handle) ) !== FALSE ) {
  array_push($full_list, $data);
}

$handle = fopen('combined.csv','r');
$combined_list = array();
while ( ($data = fgetcsv($handle) ) !== FALSE ) {
  array_push($combined_list, $data);
}

ini_set('auto_detect_line_endings',FALSE);

$final_list = array();

$count = 0;
for ($i = 0; $i < count($full_list); $i++) {
  $name = explode(" ", $full_list[$i][4]);
  foreach ($combined_list as $row) {
    if (trim(strtolower($full_list[$i][4])) == trim(strtolower($row[4]))) {
      echo "Found exact company: " . $row[4] . "\n";
      write_data($full_list[$i], $row);
      $count++;
    } else if (preg_match('/' . trim(strtolower($full_list[$i][4])) . '/', trim(strtolower($row[4])))) {
      echo "Found fuzzy company: " . $row[4] . "\n";
      write_data($full_list[$i], $row);
      $count++;
    } else if (trim(strtolower($name[0])) == trim(strtolower($row[5])) && trim(strtolower($name[1])) == trim(strtolower($row[6]))) {
      echo "Found name: " . $row[5] . " " . $row[6] . "\n";
      write_data($full_list[$i], $row);
      $count++;
    }
  }
}

echo "Total Matches: " . $count . "\n\n";

foreach ($full_list as $full) {
	clean_data($full);
  $line = implode(",", $full) . "\n";
  file_put_contents('output.csv', $line, FILE_APPEND);
}

function clean_data(&$line) {
  for ($i = 0; $i < count($line); $i++) {
    if (preg_match('/,/', $line[$i])) {
      $line[$i] = "\"" . $line[$i] . "\"";
    } else if ($line[$i] == "#N/A") {
			$line[$i] = "";
		}
  }
}

function write_data(&$full, $data) {
  $full[0] = $data[1];
  $full[1] = $data[2];
  $full[2] = $data[3];
  $full[3] = $data[4];
  $full[4] = $data[5];
  $full[5] = $data[6];
  $full[6] = $data[7];
  $full[7] = $data[8];
  $full[8] = $data[9];
  $full[9] = $data[10];
}

//foreach ($lines as $line) {
	//file_put_contents('full.csv', $line, FILE_APPEND);
//}
