<?php
$list = 'cotton';
ini_set('memory_limit', '1024M');
$list_a = array();
$file = fopen("opens_overlap.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $data = explode(',', $data);
  array_push($list_a, $data[0]);
}
fclose($file);

$list_b = array();
$file = fopen($list . "_segment.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  array_push($list_b, $data);
}
fclose($file);

unlink($list . '_segment_overlap.csv');
foreach ($list_a as $a) {
  foreach ($list_b as $b) {
    $data = explode(',', $b);
    if (trim(strtolower($a)) == trim(strtolower($data[2]))) {
      file_put_contents($list . '_segment_overlap.csv', $b, FILE_APPEND);
    }
  }
}
?>

