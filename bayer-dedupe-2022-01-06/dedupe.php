<?php
ini_set('memory_limit', '256M');

$fullList = array();

$file = fopen("full.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $full = explode(',', $data);
  $fullList[strtolower(trim($full[1])) . strtolower(trim($full[2]))] = $data;
}
fclose($file);

$count = 1;
unlink('deduped-list-2022-01-06.csv');
foreach ($fullList as $line) {
  //echo $count . ": " . $line . "\n";
  file_put_contents('deduped-list-2022-01-06.csv', $line, FILE_APPEND);
  $count++;
}
