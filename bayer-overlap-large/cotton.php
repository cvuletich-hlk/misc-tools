<?php
ini_set('memory_limit', '1024M');
$list_a = array();
$file = fopen("full_email.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $data = explode(',', $data);
  if (
    trim(strtolower($data[9])) == 'tx' ||
    trim(strtolower($data[9])) == 'ar' ||
    trim(strtolower($data[9])) == 'la' ||
    trim(strtolower($data[9])) == 'ga' ||
    trim(strtolower($data[9])) == 'ms' ||
    trim(strtolower($data[9])) == 'tn' ||
    trim(strtolower($data[9])) == 'fl' ||
    trim(strtolower($data[9])) == 'nc' ||
    trim(strtolower($data[9])) == 'sc'
    ) {
      array_push($list_a, implode(',', $data));
    } else if ($data[22] == 'TRUE' || $data[22] == 'YES') {
      array_push($list_a, implode(',', $data));
    }
}
fclose($file);

unlink('cotton_segment.csv');
foreach ($list_a as $a) {
  file_put_contents('cotton_segment.csv', $a, FILE_APPEND);
}
?>

