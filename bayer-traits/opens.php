<?php
ini_set('memory_limit', '256M');
$file = fopen("emails.csv", "r");
$count = 1;
$startdate = 1;
$enddate = 1;
$users = null;
$over = array();
$under = array();
$none = array();
while(! feof($file)) {
  $data = fgets($file);
  $data = explode(',', $data);
  $date = explode('-', $data[2], 3);
  if (($date[0] == 2020 && $date[1] > 5) || $date[0] >= 2021) {
    if (!isset($users[$data[11]])) {
      $users[$data[11]]['opens'] = $data[1];
      $users[$data[11]]['sends'] = $data[3];
      $users[$data[11]]['opens'] = $data[4];
    } else {
      $users[$data[11]]['sends'] += $data[3];
      $users[$data[11]]['opens'] += $data[4];
    }
    $count++;
  }
  //echo $data[11];
  //$line = ',' . explode(' ', $data[1], 2)[0] . ',' . explode(' ', $data[1], 2)[1] . ',,' . $data[4] . ',,' . "\n";
  //$lines[$data[1]] = $line;
}
fclose($file);

foreach ($users as $key => $user) {
  if ($user['sends'] > 0) {
    $rate = ($user['opens'] / $user['sends']) * 100;
    $line = $key . "," . $user['sends'] . "," . $user['opens'] . "," . $rate . "\n";
    if ($rate == 0) {
      file_put_contents('none.csv', $line, FILE_APPEND);
    } else if ($rate >= 50) {
      file_put_contents('more.csv', $line, FILE_APPEND);
    } else if ($rate < 50) {
      file_put_contents('less.csv', $line, FILE_APPEND);
    }
  } else {
    //$line = $key . "," . $user['opens'] . "," . $user['sends'] . ",0" . "\n";
  }
}
?>

