<?php
ini_set('memory_limit', '256M');

$fullList = array();

$file = fopen("full.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $full = explode(',', $data);
  $fullList[trim($full[2]) . trim($full[5])] = $data;
}
fclose($file);

$count = 1;
unlink('deduped-list-2021-11-12.csv');
foreach ($fullList as $line) {
  //echo $count . ": " . $line . "\n";
  file_put_contents('deduped-list-2021-11-12.csv', $line, FILE_APPEND);
  echo $line;
  $count++;
}
