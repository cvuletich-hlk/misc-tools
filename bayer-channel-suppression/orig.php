<?php
ini_set('memory_limit', '256M');

$dedupe = array();
$dedupe2 = array();

$file = fopen("full.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $addr = explode(',', $data);
  if (isset($dedupe[$addr[6]])) {
    $dedupe[$addr[6]] += intval(trim($addr[3]));
  } else {
    $dedupe[$addr[6]] = intval(trim($addr[3]));
  }
}
fclose($file);

$file = fopen("full.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $addr = explode(',', $data);
  if (isset($dedupe[$addr[6]])) {
    if ($dedupe[$addr[6]] >= 100) {
      array_push($dedupe2, $data);
      unset($dedupe[$addr[6]]);
    }
  }
}
fclose($file);

$count = 1;
foreach ($dedupe2 as $line) {
  echo $count . ": " . $line . "\n";
  file_put_contents('deduped-list-new.csv', $line, FILE_APPEND);
  $count++;
}
