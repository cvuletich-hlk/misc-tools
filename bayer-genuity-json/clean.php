<?php
error_reporting(0);
ini_set('memory_limit', '1028M');

$zipcodes = array();
$file = fopen("subscribers.json", "r");
while(! feof($file)) {
  $line = fgets($file);
  array_push($zipcodes, $line);
}
fclose($file);

$data = array();
$csv = "First Name,Last Name,Mailing State,Mailing Zip,Mailing County,Mailing Country,Corn,Soybean,Cotton,Alfalfa,Canola,Sugarbeets,Specialty,MobileCommunications,Insect Forecast,The Watch,Cotton Conversations,Pipeline Updates,Mobile,Email Address,HTML Emails,Status,Source,Acquisition Date\n";
//values and JSON values

$file = fopen("subscribers.json", "r");
while(! feof($file)) {
  $json = json_decode(fgets($file), true);
  $data = array();
  $storedlast = null;
  if ($json['emailtypepreference'] == "HTML") {
    $data['Email Preference'] = "TRUE,";
  } else {
    $data['Email Preference'] = "FALSE,";
  }
  $data['Status'] = '"' . $json['status'] . '",';
  $data['Created Date'] = $json['CreatedDate'] . ',';

  $lastflag = false;
  foreach ($json['Attributes'] as $row) {
    if ($row['Name'] == 'First Name') {
      if ($row['Value'] != '') {
        if (preg_match('/\s/', $row['Value'])) {
          list($data['First Name'], $data['Last Name']) = explode(' ', $row['Value'], 2);
          $data['First Name'] = '"' . ucwords(strtolower($data['First Name'])) . '",';
          $data['Last Name'] = '"' . ucwords(strtolower($data['Last Name'])) . '",';
          $lastflag = true;
        } else {
          $data['First Name'] = '"' . ucwords(strtolower($row['Value'])) . '",';
        }
      } else {
        $data['First Name'] = ',';
      }
    }

    if (!$lastflag) {
      if ($row['Name'] == 'Last Name') {
        if ($row['Value'] != '') {
          $data['Last Name'] = '"' . ucwords(strtolower($row['Value'])) . '",';
        } else {
          $data['Last Name'] = ',';
        }
      }
    }

    if ($row['Name'] == 'Mailing State') {
      if ($row['Value'] != '') {
        $data['Mailing State'] =  '"' . ucwords($row['Value']) . '",';
      } else {
        $data['Mailing State'] =  ',';
      }
    }

    if ($row['Name'] == 'Mailing Zip') {
      if ($row['Value'] != '') {
        $data['Mailing Zip'] =  '"' . explode("-", ucwords($row['Value']))[0] . '",';
      } else {
        $data['Mailing Zip'] = ',';
      }
    }

    if ($row['Name'] == 'Mailing County') {
      if ($row['Value'] != '') {
        $data['Mailing County'] =  '"' . ucwords(strtolower($row['Value'])) . '",';
      } else {
        $data['Mailing County'] =  ',';
      }
    }

    if ($row['Name'] == 'Shipping State') {
      if ($row['Value'] != '') {
        $data['Shipping State'] =  '"' . ucwords($row['Value']) . '",';
      } else {
        $data['Shipping State'] =  ',';
      }
    }

    if ($row['Name'] == 'Shipping Zip') {
      if ($row['Value'] != '') {
        $data['Shipping Zip'] =  '"' . explode("-", ucwords($row['Value']))[0] . '",';
      } else {
        $data['Shipping Zip'] =  ',';
      }
    }

    if ($row['Name'] == 'Corn') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['Corn'] =  ',';
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['Corn'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['Corn'] =  'TRUE,';
      }
    }

    if ($row['Name'] == 'Soybean') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['Soybean'] =  ',';
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['Soybean'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['Soybean'] =  'TRUE,';
      }
    }

    if ($row['Name'] == 'Cotton') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['Cotton'] =  ',';
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['Cotton'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['Cotton'] =  'TRUE,';
      }
    }

    if ($row['Name'] == 'Specialty') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['Specialty'] =  ',';
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['Specialty'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['Specialty'] =  'TRUE,';
      }
    }

    if ($row['Name'] == 'MobileCommunications') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['MobileCommunications'] =  ',';
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['MobileCommunications'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['MobileCommunications'] =  'TRUE,';
      } elseif (preg_match('/would like/', $row['Value'])) {
        $data['MobileCommunications'] =  'TRUE,';
      }
    }

    if ($row['Name'] == 'InsectForecast') {
      if (!$row['Value'] || $row['Value'] == '') {
        $data['InsectForecast'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'false' || strtolower($row['Value']) == 'no') {
        $data['InsectForecast'] =  'FALSE,';
      } elseif (strtolower($row['Value']) == 'true' || strtolower($row['Value']) == 'yes') {
        $data['InsectForecast'] =  'TRUE,';
      } elseif (preg_match('/consent to/', $row['Value'])) {
        $data['InsectForecast'] =  'TRUE,';
      }
    }

    if ($row['Name'] == 'Mobile Phone') {
      if ($row['Value'] != '') {
        $phone = preg_replace('/-|\(|\)|\s/', "", $row['Value']);
        if (strlen($phone) > 9) {
          $data['Mobile Phone'] = $phone . ",";
        } else {
          $data['Mobile Phone'] = ',';
        }
      } else {
        $data['Mobile Phone'] = ',';
      }
    }

    if ($row['Name'] == 'Email Address') {
      if ($row['Value'] != '') {
        $data['Email Address'] = '"' . strtolower($row['Value']) . '",';
      } else {
        $data['Email Address'] = ',';
      }
    }

    if ($row['Name'] == 'Source') {
      if (preg_match('/from Bayer/', $row['Value']) || preg_match('/from Genuity/', $row['Value'])) {
        $data['Source'] = 'Website,';
      } else {
        $data['Source'] = '"' . $row['Value'] . '",';
      }
    }

/*
    if ($data['Mailing State'] == ',') {
      foreach ($zipcodes as $line) {
        $zip = explode(',', $line);
        if ($data['Mailing Zip'] === $zip[0]) {
          $data['Mailing State'] = $zip[1] . ',';
        }
      }
    }

    foreach ($zipcodes as $line) {
      $zip = explode(',', $line);
      if ($data['Mailing Zip'] === $zip[0]) {
        $data['Mailing Country'] = $zip[5] . ',';
      }
    }
    */
  }

  //Overrides
  if ($data['Mailing State'] == "Iowa") {
    $data['Mailing State'] = 'IA';
  }

  if ($data['Mailing State'] == ",") {
    $data['Mailing State'] = $data['Shipping State'];
  }

  if ($data['Mailing Zip'] == ",") {
    $data['Mailing Zip'] = $data['Shipping Zip'];
  }

/*
  if (is_int($data['Mailing Zip']) && $data['Mailing Zip'] < 99999) {
    $data['Mailing Zip'] = ',';
  }
  */

  //CSV
  $csv .= $data['First Name'];
  $csv .= $data['Last Name'];
  $csv .= $data['Mailing State'];
  $csv .= $data['Mailing Zip'];
  $csv .= $data['Mailing County'];
  $csv .= $data['Mailing Counrty'] . ",";
  $csv .= $data['Corn'];
  $csv .= $data['Soybean'];
  $csv .= $data['Cotton'];
  $csv .= ",";
  $csv .= ",";
  $csv .= ",";
  $csv .= $data['Specialty'];
  $csv .= $data['MobileCommunications'];
  $csv .= $data['InsectForecast'];
  $csv .= ","; // The Watch
  $csv .= ","; // Cotton Conversations
  $csv .= ","; // Pipeline Updates
  $csv .= $data['Mobile Phone'];
  $csv .= $data['Email Address'];
  $csv .= $data['Email Preference'];
  $csv .= $data['Status'];
  $csv .= $data['Source'];
  $csv .= $data['Created Date'];
  $csv .=  "\n";
}

unlink('Master.csv');
file_put_contents('Master.csv', $csv, FILE_APPEND);
fclose($file);

?>
