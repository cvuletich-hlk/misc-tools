<?php
ini_set('memory_limit', '256M');

$dedupe = array();

$file = fopen("full.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $addr = explode(',', $data);
  $dedupe[trim(strtolower($addr[8])) . trim(strtolower($addr[10]))] = $data;
}
fclose($file);

$count = 1;
foreach ($dedupe as $line) {
  echo $count . ": " . $line;
  file_put_contents('deduped-list.csv', $line, FILE_APPEND);
  $count++;
}
