<?php
ini_set('memory_limit', '256M');

$fullList = array();

$file = fopen("cotton.csv", "r");
while(! feof($file)) {
  $data = fgets($file);
  $full = explode(',', $data);
  $fullList[trim($full[2]) . trim($full[5])] = $data;
}
fclose($file);

$count = 1;
unlink('deduped-cotton-2021-10-19.csv');
foreach ($fullList as $line) {
  //echo $count . ": " . $line . "\n";
  file_put_contents('deduped-cotton-2021-10-19.csv', $line, FILE_APPEND);
  echo $line;
  $count++;
}
