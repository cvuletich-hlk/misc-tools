<?php
$file = fopen('contacts.csv','r');
$contacts_list = array();
while(! feof($file)) {
  $data = fgets($file);
  array_push($contacts_list, $data);
}
fclose($file);

$file = fopen('sales.csv','r');
$sales_list = array();
while(! feof($file)) {
  $data = fgets($file);
  array_push($sales_list, $data);
}
fclose($file);

$full = array();
$count = 0;
foreach ($contacts_list as $contacts) {
  $contacts1 = explode(',', $contacts);
  foreach ($sales_list as $sales) {
    $sales1 = explode(',', $sales);
    if (strtolower(trim($contacts1[4])) == strtolower(trim($sales1[4]))) {
      $final = trim($contacts) . ',' . trim($sales) . "\n";
        echo $final;
      array_push($full, $final);
      $count++;
    }
  }
}

echo "Total Matches: " . $count . "\n\n";

foreach ($full as $full_list) {
  echo $full_list;
  //$line = implode(",", $full) . "\n";
  file_put_contents('output.csv', $full_list, FILE_APPEND);
}

